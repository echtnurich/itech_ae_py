import psutil as ps, shutil

disk = shutil.disk_usage("C:")
print("Free Disk Space:" , int(disk.free/1024/1024),"mebibyte")
print("Used Disk Space:" , int(disk.used/1024/1024),"mebibyte")
print("Total Disk Space:" , int(disk.total/1024/1024),"mebibyte")
