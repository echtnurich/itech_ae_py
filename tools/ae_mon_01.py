## 1 / 2
# sys_info.py
import os,sys,platform,getpass,socket,shutil
from datetime import datetime

userName=getpass.getuser()
userDir=os.path.expanduser('~')
osType=platform.platform()
pyVersion=platform.python_version()
cpuType=platform.processor()
hostName=socket.gethostname()
currentDir=os.getcwd()
ipAddress=socket.gethostbyname_ex(hostName)[2][-1]
now=datetime.now()
timetime=now.strftime("%Y-%m-%d %H:%M")

print("Moin!")
print("              Time:",timetime)
print("              User:",userName)
print("    User Directory:",userDir)
print("Sysinfo: ")
print("          Hostname:",hostName ,"/",ipAddress)
print("  Operating System:",osType)
print("               CPU:",cpuType)
print("    Python Version:", pyVersion)
print("  Current Directory:", currentDir)

## 3
# log_c.py
import shutil, datetime,socket

c=shutil.disk_usage("C:")
c_free=c.free
c_used=c.used
c_total=c.total
now=datetime.datetime.now()
isodatetime=now.strftime("%Y-%m-%d %H:%M")
hostName=socket.gethostname()

print(isodatetime,hostName,"  [C:] free:",c_free,"byte   used:",c_used,"byte   total:",c_total,"byte")
