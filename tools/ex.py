class throwExeption(Exception):
    def __init__(self, code, message="Unexpected Error occurred"):
        self.code = code
        self.message = message
        super().__init__(self.message)

    