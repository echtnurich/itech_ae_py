import shutil, datetime,socket

c=shutil.disk_usage("C:")
c_free=c.free
c_used=c.used
c_total=c.total
now=datetime.datetime.now()
isodatetime=now.strftime("%Y-%m-%d %H:%M")
hostName=socket.gethostname()

print(isodatetime,hostName,"  [C:] free:",c_free,"byte   used:",c_used,"byte   total:",c_total,"byte")
