import shutil, datetime,socket

c=shutil.disk_usage("C:")
c_free=c.free
c_used=c.used
c_total=c.total
now=datetime.datetime.now()
isodatetime=now.strftime("%Y-%m-%d %H:%M")
hostName=socket.gethostname()
percentFree=int(c_free/c_total*10000)/100
percentUsed=int(c_used/c_total*10000)/100


if percentUsed >= 75:
    print(isodatetime,hostName," [WARN] [C:] free:",percentFree,"% used:",percentUsed,"%")
elif percentUsed >= 90:
    print(isodatetime,hostName," [CRIT] [C:] free:",percentFree,"% used:",percentUsed,"%")
else:
    print(isodatetime,hostName," [ OK ] [C:] free:",percentFree,"% used:",percentUsed,"%")