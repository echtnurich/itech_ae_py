import shutil, datetime,socket

c=shutil.disk_usage("C:")
c_free=c.free
c_used=c.used
c_total=c.total
now=datetime.datetime.now()
isodatetime=now.strftime("%Y-%m-%d %H:%M")
hostName=socket.gethostname()
percentFree=int(c_free/c_total*10000)/100
percentUsed=int(c_used/c_total*10000)/100

print(isodatetime,hostName," [C:] free:",percentFree,"% used:",percentUsed,"%")
