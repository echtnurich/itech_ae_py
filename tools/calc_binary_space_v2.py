def calc_x(intMode,intValue,intDictValue):                      #Calculate output values based on input value and input mode
    if intMode==intDictValue:
        return intValue                                         #When input mode equals output mode
    elif intMode!=1 and intDictValue!=1:
        return int(intValue*(1024**(intMode-intDictValue)))     #When *byte <-> *byte
    elif intMode == 1:
        return int(intValue*(1024**(intMode-intDictValue+1))/8) #When bit -> *byte
    elif intDictValue == 1:
        return int(intValue*(1024**(intMode-intDictValue-1))*8) #When bit <- *byte
def print_menu(prefixDictionary):
    print("")
    for intDictValue in prefixDictionary:                       #Cycle through dictionary, priting output
        print(intDictValue,"- Enter",prefixDictionary[intDictValue])
    print("0 - Quit")                                           #Offering option to quit
    selection=int(input("Input mode: "))                        #Get Input Mode
    return selection                                            #Return Input Mode
def input_value(intMode,prefixDictionary):
    intValue = int(input("Wert in "+ prefixDictionary[intMode] +": "))#Print Input Promt and get Input Value
    return intValue                                             #Return Input Value
def print_result(intMode,intValue,prefixDictionary): 
    print("")
    for tempMode in prefixDictionary:                           #Cycle through dictionary printing results
        print("{: 20d}".format(calc_x(intMode,intValue,tempMode)),prefixDictionary[tempMode])
while True:
    prefixDict={1:"Bit",2:"Byte",3:"Kibibyte",4:"Mebibyte",5:"Gibibyte",6:"Tebibyte"}#Dictionary
    try:
        selection=print_menu(prefixDict)                        #Get Mode
        if selection==0:                                break   #Check Quit
        inputspace=input_value(selection,prefixDict)            #Get Value
        if inputspace <= 0 :                            raise ValueError#Check Error
        print_result(selection,inputspace,prefixDict)           #Print results
    except ValueError:
        print("ERROR: Only nonzero integers are supported.")
    except KeyError:
        print("ERROR: Unknown input mode.")
    else:
        print("An unexpected Error occurred.")